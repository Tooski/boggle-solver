﻿using BoggleApplication;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using BoggleApplication.Exceptions;

namespace BoggleTest {


    /// <summary>
    /// Josef Nosov
    /// This is a test class for BoggleTreeTest and is intended
    /// to contain all BoggleTreeTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BoggleTreeTest {

        /// <summary>
        /// Test dictionary words.
        /// </summary>
        string[] dictionaryWords = { "bred", "yore", "byre", "abed", "oread", "bore", "orby", "robed", "broad", "byroad", "robe", "bored", "derby", "bade", "aero", "read", "orbed", "verb", "aery", "bead", "bread", "very", "road", "wrong" };

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }


        /// <summary>
        /// Tests to see if the constructor works properly by passing an array
        /// and another that doesn't have a parameter.
        /// </summary>
        [TestMethod()]
        public void BoggleTreeConstructorTest() {
            Assert.IsTrue(new BoggleTree().TotalWords() == 0, "Default constructor should not effect total word count.");
            BoggleTree target = new BoggleTree(new string[] { "test", "" });
            Assert.IsTrue(target.TotalWords() == 1,
                        "Blank words will be removed from the dictionary");
            target = new BoggleTree(new string[] { "漢字" });
            Assert.IsTrue(target.TotalWords() == 0, "Foreign characters will not be added.");
            Assert.IsTrue(new BoggleTree(new string[] { "1234;?!." }).TotalWords() == 0,
                "1234;?!. is not valid character, will not be added.");
            Assert.IsTrue(new BoggleTree(new string[] { "123a4;?!." }).TotalWords() == 1,
                "123a4;?!. Will allow to be added due to 'a' being parsed.");
            Assert.IsTrue(new BoggleTree(new string[] { "a1234;?!." }).TotalWords() == 1,
                "a1234;?!. Will allow to be added due to 'a' being parsed.");
            Assert.IsTrue(new BoggleTree(new string[] { "1234;?!.a" }).TotalWords() == 1,
                "1234;?!.a Will allow to be added due to 'a' being parsed.");
            target = new BoggleTree(new string[] { "test", "test", "test" });
            Assert.IsTrue(target.TotalWords() == 1, "Duplicate inputs will not be added.");
            target = new BoggleTree(dictionaryWords);
            Assert.IsTrue(target.TotalWords() == dictionaryWords.Length,
                "Should be equal since we imported the words.");
            Assert.IsFalse(target.TotalWords() == dictionaryWords.Length - 1
                && target.TotalWords() == dictionaryWords.Length + 1,
                "Verifying that total words are exactly dictionaryWord's length.");
            Assert.IsTrue(new BoggleTree(new string[6]).TotalWords() == 0, "Will equal 0 since string[] have empty values.");
            bool exception_success = false;
            try {
                Assert.Fail("Will fail due to null exception being thrown.", new BoggleTree(null));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Will fail due to string array being initalized to length 0.", new BoggleTree(new string[0]));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
        }

       /// <summary>
       /// Tests the add method within BoggleTree to make sure it adds and updates
       /// the count properly.
       /// </summary>
        [TestMethod()]
        public void AddTest() {
            BoggleTree target = new BoggleTree();
            target.Add(string.Empty);
            target.Add("");
            target.Add(null);
            target.Add("");
            Assert.IsTrue(target.TotalWords() == 0, "Should not add null or empty strings to the dictionary");
            target.Add("word");
            Assert.IsTrue(target.TotalWords() == 1, "Adds word successfully.");
            target.Add("wo rd");
            Assert.IsTrue(target.TotalWords() == 1, "Word will not be added since the space is " +
                "removed leaving 'word', which has previously been added.");
            target.Add("12341");
            Assert.IsTrue(target.TotalWords() == 1, "Word will not be added due to numeric characters");
            target.Add("12341a");
            Assert.IsTrue(target.TotalWords() == 2, "'a' will be parsed and added to the list");
            target.Add("12b341");
            Assert.IsTrue(target.TotalWords() == 3, "'b' will be parsed and added to the list");
            target.Add("c12341");
            Assert.IsTrue(target.TotalWords() == 4, "'c' will be parsed and added to the list");

        }


        /// <summary>
        /// Thoughroughly tests the search method to see if it can find the exact results
        /// based on the words imported into the boggle tree.
        /// </summary>
        [TestMethod()]
        public void SearchTest() {
            BoggleTree tree = new BoggleTree(dictionaryWords);
            Assert.IsTrue(tree.Search(null) == BoggleTree.SearchResults.NotExists, "null returns empty value");
            Assert.IsTrue(tree.Search(string.Empty) == BoggleTree.SearchResults.NotExists, "empty string returns not exists.");
            Assert.IsTrue(tree.Search("") == BoggleTree.SearchResults.NotExists, "Blank space does not exist");
            Assert.IsTrue(tree.Search("bred") == BoggleTree.SearchResults.Exists, "Will return in existing value.");
            Assert.IsTrue(tree.Search("Bred") == BoggleTree.SearchResults.Exists, "Capital letters shouldn't search.");
            Assert.IsTrue(tree.Search("B!r4e.d") == BoggleTree.SearchResults.Exists, "Removes any non alphabetic characters. Should result in bred.");
            Assert.IsTrue(tree.Search("x") == BoggleTree.SearchResults.NotExists, "There are no prefix letters that start with x");
            Assert.IsTrue(tree.Search("ro") == BoggleTree.SearchResults.Prefix, "Would return prefix because it makes up the word 'robe' and 'robed'");
            Assert.IsTrue(tree.Search("robe") == BoggleTree.SearchResults.PrefixAndExists, "Is both a prefix to 'robed' to another word and an existing word.");
            Assert.IsTrue(tree.Search("robed") == BoggleTree.SearchResults.Exists, "'robed' is in the leaf node, value returned exists");
            Assert.IsTrue(tree.Search("漢字") == BoggleTree.SearchResults.NotExists, "Will return not exist if foreign characters are input.");
        }
    }
}
