﻿using BoggleApplication;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using BoggleApplication.Exceptions;

namespace BoggleTest {

    /// <summary>
    /// Josef Nosov
    /// 
    /// This is a test class for BoggleBoardTest and is intended
    /// to contain all BoggleBoardTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BoggleBoardTest {

        /// <summary>
        /// 3x3 test board.
        /// </summary>
        private string[,] boggleBoard = new string[,] { { "y", "o", "x" }, { "r", "b", "a" }, { "v", "e", "d" } };

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }


        /// <summary>
        /// Tests the nodes, making sure it recieves the proper characters.
        /// </summary>
        [TestMethod()]

        public void BoggleNodeConstructorTest() {
            Assert.AreNotEqual(new BoggleNode("k").Character, "K", "Lowercase k should not be equal to uppercase K.");
            Assert.AreNotEqual(new BoggleNode("k").Character, "j", "Should not be equal k != j");
            Assert.AreEqual(new BoggleNode("k").Character, "k", "Should be equal, k == k");

            /// Try/catches to prevent tests from ending prematurely.
            bool exception_success = false;
            try {
                Assert.Fail("Will return an exception due to a null string.", new BoggleNode(null));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Will return an exception due to empty string.", new BoggleNode(""));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
        }

        /// <summary>
        /// Tests the board constructor, makes sure the neigbors are all detectable.
        /// </summary>
        [TestMethod()]
        public void BoggleBoardConstructorTest() {

            BoggleBoard board = new BoggleBoard(boggleBoard);
            int[] neighbor = { -1, 0, 1 };

            /// Finds all possible neighbors for all nodes, if found, returns true, else false.
            /// False will cause the test to fail.
            foreach (BoggleNode node in board.neighborsOf.Keys) {
                int k = 0;
                for (; k < boggleBoard.GetLength(0) * boggleBoard.GetLength(1); k++) {
                    if (node.Character == boggleBoard[k % boggleBoard.GetLength(0), k / boggleBoard.GetLength(1)]) break;
                }
                int x = k % boggleBoard.GetLength(0);
                int y = k / boggleBoard.GetLength(1);
                foreach (BoggleNode neighbors in board.neighborsOf[node]) {
                    bool foundNeighbor = false;
                    for (int i = 0; i < neighbor.Length; i++) {
                        for (int j = 0; j < neighbor.Length; j++) {
                            if (x + neighbor[i] < boggleBoard.GetLength(0) && x + neighbor[i] >= 0 &&
                               y + neighbor[j] < boggleBoard.GetLength(1) && y + neighbor[j] >= 0)
                                if (boggleBoard[x + neighbor[i], y + neighbor[j]].Equals(neighbors.Character)) {
                                    foundNeighbor = true;
                                }
                        }
                    }
                    Assert.IsTrue(foundNeighbor, "Should return true when neighbor is detected.");
                }
            }

            BoggleBoard testBoard = new BoggleBoard(new string[,] { { "a" } });

            foreach (BoggleNode b in testBoard.neighborsOf.Keys) {
                Assert.AreEqual(testBoard.neighborsOf[b].Count, 0, "Should be 0 since 1x1 will have no neighbors");
                Assert.AreNotEqual(testBoard.neighborsOf[b].Count, 1, "Any value besides 0 should break.");
            }

            testBoard = new BoggleBoard(new string[,] { { "a", "b" }, { "c", "d" } });
            foreach (BoggleNode b in testBoard.neighborsOf.Keys) {
                Assert.AreEqual(testBoard.neighborsOf[b].Count, 3, "a, b, c, d will be neighbors of each other within a 2x2 block. Result should be 3");
                Assert.AreNotEqual(testBoard.neighborsOf[b].Count, 1, "Any value besides 3 should break.");
            }

            /// Try/catches to prevent tests from ending prematurely.
            bool exception_success = false;
            try {
                Assert.Fail("Will cause an exception if array passed in is null.", new BoggleBoard(null));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Exception will be thrown if the width is not set.", new BoggleBoard(new string[5, 0]));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Exception will be thrown if the height is not set.", new BoggleBoard(new string[0, 5]));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
        }
    }
}
