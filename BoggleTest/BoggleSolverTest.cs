﻿using BoggleApplication;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Collections.Generic;
using BoggleApplication.Exceptions;

namespace BoggleTest {


    /// <summary>
    /// Josef Nosov
    /// This is a test class for BoggleSolverTest and is intended
    /// to contain all BoggleSolverTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BoggleSolverTest {

        /// <summary>
        /// A test dictionary with 24 different words.
        /// </summary>
        private string[] dictionaryWords = { "bred", "yore", "byre", "abed", "oread", "bore", "orby", "robed", "broad", "byroad", "robe", "bored", "derby", "bade", "aero", "read", "orbed", "verb", "aery", "bead", "bread", "very", "road", "wrong" };

        /// <summary>
        /// Expected words within a hashset, contains 23 different words.
        /// </summary>
        private HashSet<string> expected = new HashSet<string>(new string[]{ "bred", "yore", 
                "byre", "abed", "oread", "bore", "orby", "robed", "broad", "byroad", "robe", 
                "bored", "derby", "bade", "aero", "read", "orbed", "verb", "aery", "bead",
                "bread", "very", "road"});
        
        /// <summary>
        /// A test board, 3x3.
        /// </summary>
        private string[,] board = new string[,] { { "y", "o", "x" }, { "r", "b", "a" }, { "v", "e", "d" } };


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Tests the default BoggleSolver's solve method. Accepts width, height, board and boggle tree.
        /// This will allow users to set width and height as well as pass in board. if the board is
        /// null or doesn't have the same width or height as the board it creates a new board and
        /// places the values into similar positions.
        /// </summary>
        [TestMethod()]
        public void SolveTest() {

            BoggleTree boggle = new BoggleTree(dictionaryWords);
            HashSet<string> actual = BoggleSolver.Solve(3, 3, board, boggle);
            foreach (string word in actual) {
                Assert.IsTrue(expected.Contains(word),
                    "Each word exists within the dictionary.");
            }

            actual = BoggleSolver.Solve(5, 5, board, boggle);
            foreach (string word in expected) {
                Assert.IsTrue(actual.Contains(word),
                    "Width and height is changed.");
            }
            Assert.IsTrue(actual.Count >= expected.Count, "Expected will be a subset of actual result.");

            actual = BoggleSolver.Solve(2, 2, board, boggle);
            Assert.IsTrue(actual.Count == 1, "Only word that will return will be orby, from");
            // [ y ][ o ]
            // [ r ][ b ]
            Assert.IsTrue(actual.Contains("orby"), "Orby is the only word from the actual list.");
            Assert.IsFalse(actual.Contains("bred"), "Bred is not within actual list.");

            actual = BoggleSolver.Solve(2, 3, board, boggle);
            Assert.IsTrue(actual.Count == 7);
            foreach (string str in new string[] { "yore", "orby", "robe", "byre", "bore", "very", "verb" }) {
                Assert.IsTrue(actual.Contains(str), "These are substring.");
            }
            Assert.IsTrue(BoggleSolver.Solve(3, 3, new string[0, 0], boggle).Count >= 0, "Not null, width and height initalizes the board.");
            Assert.IsTrue(BoggleSolver.Solve(3, 3, new string[3, 3], boggle).Count >= 0, "New board puts random values into the array.");
            Assert.IsTrue(BoggleSolver.Solve(3, 3, null, boggle).Count >= 0, "Initalizes the board based on height and width.");

            /// Try/catches to prevent tests from ending prematurely.
            bool exception_success = false;
            try {
                Assert.Fail("BoggleTree has no values, will throw exception.", BoggleSolver.Solve(3, 3, new string[3, 3], new BoggleTree()));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Height is 0, will throw exception.", BoggleSolver.Solve(3, 0, board, boggle));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Width is 0, will throw exception", BoggleSolver.Solve(0, 3, board, boggle));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Width and height is 0, will throw exception", BoggleSolver.Solve(0, 0, board, boggle));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("width and height are set to 0, array width and height is 0, will throw exception.",
                    BoggleSolver.Solve(0, 0, new string[0, 0], boggle));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
        }

        /// <summary>
        /// This test will create a board based on width and height, it will fill
        /// the board with random values, best bet is to make sure the actual count
        /// is greater or equal to 0, that way we can ensure that the BoggleSolver 
        /// returned a non-null value and didn't crash. There is really no other way
        /// to guarentee the test success every run due to the randomness factor.
        /// </summary>
        [TestMethod()]
        public void SolveTest1() {
            HashSet<string> actual = BoggleSolver.Solve(30, 30, dictionaryWords);
            Assert.IsTrue(actual.Count >= 0, "Contains all values from expected.");


            /// Try/catches to prevent tests from ending prematurely.
            bool exception_success = false;
            try {
                Assert.Fail("Dictionary is empty, will throw exception", BoggleSolver.Solve(20, 20, new string[0]));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Dictionary is null, will throw exception", BoggleSolver.Solve(20, 20, null));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Will fail due to 0 throwing exception", BoggleSolver.Solve(0, 20, dictionaryWords));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Will fail due to 0 throwing exception", BoggleSolver.Solve(20, 0, dictionaryWords));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Will fail due to 0 throwing exception", BoggleSolver.Solve(0, 0, dictionaryWords));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
        }

        /// <summary>
        /// Tests the method that accepts width, height, the board, and a dictionary
        /// of string array. This does the same as the above test, but uses an array instead
        /// which gets placed into a the boggle tree.
        /// </summary>
        [TestMethod()]

        public void SolveTest2() {
        
            HashSet<string> actual = BoggleSolver.Solve(3, 3, board, dictionaryWords);
            Assert.IsTrue(actual.Count == 23, "Contains all values from expected.");
            foreach (string results in expected) {
                Assert.IsTrue(actual.Contains(results), "expected is a subset of actual.");
            }


            actual = BoggleSolver.Solve(50, 50, board, dictionaryWords);
            Assert.IsTrue(actual.Count >= 23, "For something larger, the boggle board expands but the" +
            "boards values stay the same.");
            foreach (string results in expected) {
                Assert.IsTrue(actual.Contains(results), "expected is a subset of actual.");
            }

            actual = BoggleSolver.Solve(2, 2, board, dictionaryWords);
            Assert.IsTrue(actual.Count == 1, "orby is the only result.");
            Assert.IsTrue(actual.Contains("orby"), "orby should be the only result.");
            Assert.IsTrue(BoggleSolver.Solve(20, 20, null, dictionaryWords).Count >= 0, "Null value is created with width and length");
            Assert.IsTrue(BoggleSolver.Solve(20, 20, new string[0, 0], dictionaryWords).Count >= 0, "The new string will be replaced" +
                "with a new array of dimensions 20x20.");

            /// Try/catches to prevent tests from ending prematurely.
            bool exception_success = false;
            try {
                Assert.Fail("Dictionary is empty, will throw exception", BoggleSolver.Solve(20, 20, board, new string[0]));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Will fail due to 0 throwing exception", BoggleSolver.Solve(0, 20, board, dictionaryWords));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Will fail due to 0 throwing exception", BoggleSolver.Solve(20, 0, board, dictionaryWords));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Will fail due to 0 throwing exception", BoggleSolver.Solve(0, 0, board, dictionaryWords));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
        }

        /// <summary>
        /// This test will use a boggle tree that imports dictionary words.
        /// This tests tests to make sure all input and output is correct.
        /// It even creates a board that is 200x200 and checks the dictionary
        /// to see if those words exists. The test has an extremely microscopic
        /// chance of failing, but not getting one of the 24 words in a 40000 combination
        /// seems highly unlikely.
        /// </summary>
        [TestMethod()]
       
        public void SolveTest3() {

            BoggleTree dictionary = new BoggleTree(dictionaryWords);
            HashSet<string> actual = BoggleSolver.Solve(board, dictionary);
            Assert.IsTrue(actual.Count == 23, "Contains all values from expected.");
            foreach (string results in expected) {
                Assert.IsTrue(actual.Contains(results), "expected is a subset of actual.");
            }

            actual = BoggleSolver.Solve(new string[200, 200], dictionary);
            Assert.IsTrue(actual.Count >= 0, "probability of finding all 24 words within" +
                "the 200x200 random space is high.");
            foreach (string results in actual) {
                if (results.Equals("wrong")) {
                    Assert.IsFalse(expected.Contains(results), "wrong should not exist within the subset.");
                } else {
                    Assert.IsTrue(expected.Contains(results), "expected is a subset of actual.");
                }
            }

            string[,] oldArray = board;
            string[,] newArray = new string[6, 6];
            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 6; j++) {
                    if (oldArray.GetLength(1) > i && oldArray.GetLength(0) > j) {
                        newArray[i, j] = oldArray[i, j];
                    } else {
                        newArray[i, j] = BoggleDice.RandomChar();
                    }
                }
            }
            actual = BoggleSolver.Solve(oldArray, dictionary);
            Assert.IsTrue(actual.Count >= 23, "The space changed to a 6x6 which means that there"
                + "are going to be 9 spaces that remained the same from the previous array"
                + "and 27 spaces are going to have random characters.");
            foreach (string results in expected) {
                Assert.IsTrue(actual.Contains(results), "expected is a subset of actual.");
            }

            /// Try/catches to prevent tests from ending prematurely.
            bool exception_success = false;
            try {
                Assert.Fail("Will throw exception due to dictionary being empty.", BoggleSolver.Solve(board, new BoggleTree()));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Will throw exception due to array being 0x5.", BoggleSolver.Solve(new string[0, 5], dictionary));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Will throw exception due to array being 0x5.", BoggleSolver.Solve(new string[5, 0], dictionary));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("The board is null, cannot instantiate without width/height.", BoggleSolver.Solve(null, dictionary));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Will throw exception due to array being 0x0.", BoggleSolver.Solve(new string[0, 0], dictionary));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;

        }
            
        

        /// <summary>
        /// Tests the solve method, makes sure the results all work properly.
        /// If you are reading this, you are a cool individual.
        /// </summary>
        [TestMethod()]
        public void SolveTest4() {

            HashSet<string> actual = BoggleSolver.Solve(board, dictionaryWords);
            Assert.IsTrue(actual.Count == 23, "Contains all values from expected.");
            foreach (string word in actual) {
                Assert.IsTrue(expected.Contains(word),
                    "Expected is a subset of actual");
            }

            string[,] oldArray = board;
            string[,] newArray = new string[6, 6];
            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 6; j++) {
                    if (oldArray.GetLength(1) > i && oldArray.GetLength(0) > j) {
                        newArray[i, j] = oldArray[i, j];
                    } else {
                        newArray[i, j] = BoggleDice.RandomChar();
                    }
                }
            }

            actual = BoggleSolver.Solve(newArray, dictionaryWords);
            Assert.IsTrue(actual.Count >= 23, "Contains all values from expected.");
            foreach (string word in actual) {
                Assert.IsTrue(expected.Contains(word),
                    "Expected is a subset of actual");
            }

            /// Try/catches to prevent tests from ending prematurely.
            bool exception_success = false;
            try {
                Assert.Fail("Will throw exception due to array being 0x5.", BoggleSolver.Solve(new string[0, 5], dictionaryWords));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Will throw exception due to array being 0x5.", BoggleSolver.Solve(new string[5, 0], dictionaryWords));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("The board is null, cannot instantiate without width/height.", BoggleSolver.Solve(null, dictionaryWords));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Will throw exception due to dictionary being empty.", BoggleSolver.Solve(board, new string[0]));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
        }
    }
}
