﻿using BoggleApplication;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using BoggleApplication.Exceptions;

namespace BoggleTest {


    /// <summary>
    /// Josef Nosov
    /// 
    /// This is a test class for BoggleDiceTest and is intended
    /// to contain all BoggleDiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BoggleDiceTest {
        /// <summary>
        /// The 3x3 test board.
        /// </summary>
        private string[,] boggleBoard = new string[,] { { "y", "o", "x" }, { "r", "b", "a" }, { "v", "e", "d" } };
        /// <summary>
        /// Regular expression for all letters that are lowercase a-z.
        /// </summary>
        private Regex regA = new Regex("[a-z]");
        /// <summary>
        /// Regular expression for anything other than letters that are lowercase a-z
        /// </summary>
        private Regex regNA = new Regex("[^a-z]");

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Tests to see if the random boggle characters actually return alphabetic lowercase characters.
        /// </summary>
        [TestMethod()]
        public void RandomBoggleCharactersTest() {
            string[] actual = BoggleDice_Accessor.RandomBoggleCharacters();
            Assert.IsTrue(actual.Length > 0, "List of possible characters must be greater than 0");
            HashSet<string> uniqueValues = new HashSet<string>();
            foreach (string character in actual) {
                Assert.IsTrue(regA.IsMatch(character), "Random variables must be alphabetic.");
                Assert.IsFalse(regNA.IsMatch(character), "Random variables cannot be anything but alphabetic.");
                Assert.IsTrue(character.Length == 1 || (character.Equals("qu") && character.Length == 2),
                    "character should be the length of 1, only exception is qu.");
                uniqueValues.Add(character);
            }
            Assert.IsTrue(uniqueValues.Count == 26, "Makes sure there are 26 different values, a through z (q = qu)");
            Assert.IsFalse(uniqueValues.Count != 26, "If value is anything but 26 then something went wrong.");
        }

        /// <summary>
        /// Tests the random dice method, this will take in the board and place random dice
        /// to any of the empty spots on the board.
        /// </summary>
        [TestMethod()]
        public void RandomDiceTest() {
            int dimension = 5;
            string[,] dies = BoggleDice.RandomDice(new string[dimension, dimension]);
            Assert.IsTrue(dies.GetLength(0) == dimension && dies.GetLength(1) == dimension,
                "RandomDice method shouldn't change dimensions of the board.");

            for (int i = 0; i < dimension * dimension; i++) {
                string ch = dies[i / dimension, i % dimension];
                Assert.IsTrue(regA.IsMatch(ch), "Random variables must be alphabetic.");
                Assert.IsFalse(regNA.IsMatch(ch), "Random variables cannot be anything but alphabetic.");
                Assert.IsTrue(ch.Length == 1 || (ch.Equals("qu") && ch.Length == 2),
                  "character should be the length of 1, only exception is qu.");
            }

            dies = BoggleDice.RandomDice((string[,])boggleBoard.Clone());
            int x = boggleBoard.GetLength(0);
            int y = boggleBoard.GetLength(1);
            for (int i = 0; i < y * x; i++) {
                string ch = dies[i / y, i % x];
                Assert.IsTrue(ch.Equals(boggleBoard[i / y, i % x]),
                    "If there are already variables in the array, then it will not replace with a random variable.");
            }

            dies = new string[dimension, y]; // 5 x 3
            for (int i = 0; i < x * y; i++) {
                dies[i / y, i % x] = boggleBoard[i / y, i % x];
            }

            for (int i = 0; i < dimension; i++) {
                for (int j = 0; j < y; j++) {
                    if (i < x && j < y) {
                        Assert.IsNotNull(dies[i, j], "should not be null, added above.");
                        Assert.AreEqual(dies[i, j], boggleBoard[i, j], "Values should equal the same");
                    } else {
                        Assert.IsNull(dies[i, j], "should be null, values not added.");
                    }
                }
            }

            dies = BoggleDice.RandomDice(dies);
            for (int i = 0; i < dimension; i++) {
                for (int j = 0; j < y; j++) {
                    Assert.IsNotNull(dies[i, j], "All nodes should be filled in, random and otherwise.");
                    if (i < x && j < y) {
                        Assert.AreEqual(dies[i, j], boggleBoard[i, j], "Values should remain unaltered");
                    } else {
                        Assert.IsTrue(regA.IsMatch(dies[i, j]), "Random variables, testing to make sure all are characters.");
                    }
                }
            }

            /// Try/catches to prevent tests from ending prematurely.
            bool exception_success = false;
            try {
                Assert.Fail("Null will throw an exception.", BoggleDice.RandomDice(null));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Width is 0, needs to be greater than 0.", BoggleDice.RandomDice(new string[5, 0]));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Height is 0, needs to be greater than 0.", BoggleDice.RandomDice(new string[0, 5]));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
            try {
                Assert.Fail("Both width and height are 0.", BoggleDice.RandomDice(new string[0, 0]));
            } catch (NullEmptyZeroException e) {
                exception_success = !string.IsNullOrEmpty(e.ToString());
            }
            Assert.IsTrue(exception_success, "If above exception occured, then isTrue.");
            exception_success = false;
        }

        /// <summary>
        /// Checks to see if the character returned are valid, checking both the length and if
        /// the format is correct.
        /// </summary>
        [TestMethod()]
        public void RandomCharTest() {
            Assert.IsTrue(regA.IsMatch(BoggleDice.RandomChar()), "Should match alphabetic regular expression");
            Assert.IsFalse(regNA.IsMatch(BoggleDice.RandomChar()), "Should not be anything but lowercase random alphabetic value.");

            for (int i = 0; i < 100; i++) {
                string dice = BoggleDice.RandomChar();
                Assert.IsTrue(dice.Length == 1 || (dice.Equals("qu") && dice.Length == 2),
                "character should be the length of 1, only exception is qu.");
            }
        }
    }
}
