﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Diagnostics;

namespace BoggleApplication {
    /// <summary>
    /// Josef Nosov
    /// 
    /// The Control class that communicates to the Boggle Solver to complete the task.
    /// </summary>
    public partial class BoggleForm : System.Web.UI.Page {


        /// <summary>
        /// Initalizes the variable for the page, ensuring that the session stays
        /// persistant throughout the runtime of the application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="r"></param>
        protected void Page_PreInit(object sender, EventArgs r) {
            if (Session["dictionaryTree"] == null) {
                if (Session["board"] == null) Session["board"] = new string[,] { { "y", "o", "x" }, { "r", "b", "a" }, { "v", "e", "d" } };
                Session["height"] = Int32.Parse(Height.Text);
                Session["width"] = Int32.Parse(Width.Text);
                setupTable((int)Session["width"], (int)Session["height"]);
                for (int i = 0; i < (int)Session["height"]; i++) {
                    for (int j = 0; j < (int)Session["width"]; j++) {
                        ((TextBox)Session[i + "|" + j]).Text = ((string[,])Session["board"])[i, j];
                    }
                }
            }
            int width = (int)Session["width"];
            int height = (int)Session["height"];
            if (width == ((string[,])Session["board"]).GetLength(1)
                && height == ((string[,])Session["board"]).GetLength(0)) {
                setupTable(width, height);
            }

        }

        /// <summary>
        /// Creates a new dictionary based on the input file.
        /// </summary>
        /// <param name="reader">The file with the dictionary words.</param>
        private void createDictionary(StreamReader reader) {
            Session["dictionaryTree"] = new BoggleTree(Regex.Split(reader.ReadToEnd(), "\n"));
            reader.Close();
        }

        /// <summary>
        /// Handles the files, makes sure formatted properly.
        /// Prints the exception in ImportException if the file is
        /// invalid or no file submitted.
        /// </summary>
        private bool FileHandler() {
            if (DictionaryImport.HasFile) {
                if (Path.GetExtension(DictionaryImport.FileName).Equals(".txt")) {
                    createDictionary(new StreamReader(DictionaryImport.FileContent));
                    Session["dictionary"] = DictionaryImport.FileName;
                } else {
                    ImportException.Text = "Invalid file extension! Please import a *.txt file.";
                }
            } else {
                ImportException.Text = string.Empty;
            }
            if (Session["dictionary"] == null) {
                ImportException.Text = "Please import a valid file!";
            } else {
                currentDictionary.Text = Session["dictionary"].ToString() + " - " + ((BoggleTree)Session["dictionaryTree"]).TotalWords() + " words";
            }
            return ImportException.Text == string.Empty;
        }

        /// <summary>
        /// Updates table based on the width and height. If width and height is greater 
        /// than the previous table, it adds random characters.
        /// </summary>
        private void TableUpdater() {
            if (Width.Text.Length > 0) {
                Session["width"] = Int32.Parse(Width.Text);
            } else {
                Width.Text = Session["width"].ToString();
            }
            if (Height.Text.Length > 0) {
                Session["height"] = Int32.Parse(Height.Text);
            } else {
                Height.Text = Session["height"].ToString();
            }
            /// Makes sure that the value inside of cell is an alphabetic value.
            /// If not, then it selects a random character.
            Regex reg = new Regex("[a-zA-Z]");
            string[,] board = ((string[,])Session["board"]);
            for (int i = 0; i < board.GetLength(0); i++) {
                for (int j = 0; j < board.GetLength(1); j++) {
                    if (Session[i + "|" + j] != null && ((TextBox)Session[i + "|" + j]).Text.Length > 0) {
                        string characterValue = ((TextBox)Session[i + "|" + j]).Text;
                        if (reg.IsMatch(characterValue)) {
                            board[i, j] = characterValue;
                        } else {
                            board[i, j] = BoggleDice.RandomChar();
                        }
                    }
                }
            }
            setupTable((int)Session["width"], (int)Session["height"]);
        }


        /// <summary>
        /// Based on selection, the update will either give random dice or 
        /// </summary>
        private void SelectionHandler() {
            int width = (int)Session["width"];
            int height = (int)Session["height"];
            if (InputType.SelectedValue == "Randomize!") {
                Session["board"] = BoggleDice.RandomDice(new string[height, width]);

            } else {
                string[,] oldArray = ((string[,])Session["board"]);
                string[,] newArray = new string[height, width];
                for (int i = 0; i < height; i++) {
                    for (int j = 0; j < width; j++) {
                        if (oldArray.GetLength(0) > i && oldArray.GetLength(1) > j) {
                            newArray[i, j] = oldArray[i, j];
                        } else {
                            newArray[i, j] = BoggleDice.RandomChar();
                        }

                    }
                }
                Session["board"] = newArray;
            }
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (Session[i + "|" + j] != null)
                        ((TextBox)Session[i + "|" + j]).Text = ((string[,])Session["board"])[i, j];
                }
            }
        }

        /// <summary>
        /// Finds all the words and updates with results.
        /// </summary>
        private void WordResults() {
            StringBuilder sb = new StringBuilder();
            sb.Append("Results: ");
            HashSet<string> foundWords = BoggleSolver.Solve(((string[,])Session["board"]), ((BoggleTree)Session["dictionaryTree"]));
            int commas = 0;
            foreach (string words in foundWords) {
                sb.Append(words);
                if (++commas != foundWords.Count)
                    sb.Append(", ");
            }
            if (foundWords.Count == 0) sb.Append("None");
            else sb.Append("<br>" + foundWords.Count + (foundWords.Count == 1 ? " word has" : " words have") + " been found!");
            Results.Text = sb.ToString();
        }

        /// <summary>
        /// Imports the dictionary, updates logic, updates table and returns the result based on inputs.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Solver(object sender, EventArgs e) {

            if (FileHandler()) {
                TableUpdater();
                SelectionHandler();
                WordResults();
            }
        }

        /// <summary>
        /// Dynamically sets up the table with given characters.
        /// </summary>
        /// <param name="width">The required width of the table.</param>
        /// <param name="height">The required height of the table.</param>
        private void setupTable(int width, int height) {

            // Removes any unnecessary rows if the height has been changed.
            while (BoggleTable.Rows.Count > height) BoggleTable.Rows.RemoveAt(BoggleTable.Rows.Count - 1);

            // Removes any unncessary columns if the width has been changed.
            for (int i = 0; i < BoggleTable.Rows.Count; i++) {
                while (BoggleTable.Rows[i].Cells.Count > width) BoggleTable.Rows[i].Cells.RemoveAt(BoggleTable.Rows[i].Cells.Count - 1);
            }

            // Adds rows that needs to be added.
            for (int i = BoggleTable.Rows.Count; i < height; i++) {
                BoggleTable.Rows.Add(new TableRow());
            }

            // Adds columns that need to be added along with a dynamically created textbox.
            for (int i = 0; i < height; i++) {
                for (int j = BoggleTable.Rows[i].Cells.Count; j < width; j++) {
                    TextBox boggleCharacter = new TextBox();
                    boggleCharacter.Width = 25;
                    boggleCharacter.MaxLength = 2;
                    boggleCharacter.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                    TableCell cell = new TableCell();
                    cell.Controls.Add(boggleCharacter);
                    cell.HorizontalAlign = HorizontalAlign.Center;
                    BoggleTable.Rows[i].Cells.Add(cell);
                    Session[i + "|" + j] = boggleCharacter;

                }
            }
        }
    }
}