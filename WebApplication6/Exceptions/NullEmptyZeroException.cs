﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoggleApplication.Exceptions {

    /// <summary>
    /// Josef Nosov
    /// 
    /// Throws exception if the requested values were incorrectly formatted.
    /// </summary>
    public class NullEmptyZeroException : Exception {

        /// <summary>
        /// Exception is thrown when the value is either null, is an empty array/string,
        /// or is a numeric value equal to 0.
        /// </summary>
        public NullEmptyZeroException() : base("The value must have instantiated, cannot be empty, and cannot be zero.") { }

        /// <summary>
        /// Exception is thrown with a provided message when the value is either 
        /// null, is an empty array/string, or is a numeric value equal to 0. 
        /// </summary>
        public NullEmptyZeroException(string exception) : base(exception) {}
    }
}