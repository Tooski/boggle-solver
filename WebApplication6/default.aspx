﻿<%-- Created By: Josef Nosov --%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="BoggleApplication.BoggleForm"
    EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Boggle Solver - By Josef Nosov</title>
    <script type="text/javascript">
        /// The updateInput function prevents users from inputting 
        /// a non-numeric value, non-zero and non-negative integer value.
        function updateInput(value) {
            var text = document.getElementById(value);
            text.value = text.value.replace(/[^0-9]/g, "");
            if (text.value === "0") text.value = "1";
        }
    </script>
</head>
<body runat="server">
    <center>
        <h2>
            Boggle Solver</h2>
        <form id="BoggleForm" runat="server">
        <%-- Dynamically updated table with boggle characters --%>
        <asp:Table ID="BoggleTable" runat="server">
        </asp:Table>
        <%-- How the inputs are managed, either inputs or random variables --%>
        Input Values:
        <asp:RadioButtonList ID="InputType" runat="server">
            <asp:ListItem>Randomize!</asp:ListItem>
            <asp:ListItem Selected="True">Selected Values</asp:ListItem>
        </asp:RadioButtonList>
        <%-- User can set width and height. --%>
        Width:
        <asp:TextBox ID="Width" runat="server" MaxLength="2" Width="35px" onkeypress="updateInput('Width')"
            onkeyup="updateInput('Width')" Text="3"></asp:TextBox>
        Height:
        <asp:TextBox ID="Height" runat="server" MaxLength="2" Width="35px" onkeypress="updateInput('Height')"
            onkeyup="updateInput('Height')" Text="3"></asp:TextBox>
        <br />
        <%-- The currently selected dictionary --%>
        Current Dictionary:
        <asp:Label ID="currentDictionary" runat="server"></asp:Label>
        <br />
        <%-- Imports .txt file with list of words --%>
        <asp:FileUpload ID="DictionaryImport" runat="server" accept=".txt" />
        <br />
        <%-- Exception if attempts to import non-*.txt file --%>
        <asp:Label ID="ImportException" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
        <p />
        <%-- The meat of the application. Solves all world's problems. --%>
        <asp:Button ID="Solve" runat="server" OnClick="Solver" Text="Solve!" />
        <p />
        <%-- The resulting words.--%>
        <asp:Label ID="Results" runat="server"></asp:Label>
        </form>
    </center>
</body>
</html>
