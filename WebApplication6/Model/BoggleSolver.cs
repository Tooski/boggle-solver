﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoggleApplication;
using BoggleApplication.Exceptions;


namespace BoggleApplication {

    /// <summary>
    /// Josef Nosov
    /// 
    /// Boggle Solver is a utility class that verifies all 
    /// parameters are valid before running the search.
    /// Returns a hashset of words found within the tree.
    /// 
    /// </summary>
    class BoggleSolver {

        /// <summary>
        /// The dictionary in a tree data structure.
        /// </summary>
        private static BoggleTree _dictionaryTree = null;

        /// <summary>
        /// Creates a random board based on width and height parameters. Returns a list of found words.
        /// <param name="dictionaryWords">The list of words imported from dictionary source.</param>
        /// <param name="width">The width of the board, must be greater than 0</param>
        /// <param name="height">The height of the board, must be greater than 0</param>
        /// </summary>
        public static HashSet<string> Solve(int width, int height, string[] dictionaryWords) {
            if (width <= 0 || height <= 0) {
                throw new NullEmptyZeroException(
                    width <= 0 ? height <= 0 ? "Width and Height are" : "Width is" : "Height is" +
                    " less than or equal to 0.");
            }
            return Solve(width, height, null, new BoggleTree(dictionaryWords));
        }

        /// <summary>
        /// Uses the board to find all possible words for boggle.
        /// <param name="dictionaryWords">The list of words imported from dictionary source.</param>
        /// <param name="board">The board that will be used for </param>
        /// </summary>
        public static HashSet<string> Solve(string[,] board, BoggleTree dictionaryWords) {
            if (board == null) {
                throw new NullEmptyZeroException("Board is not initalized.");
            }
            return Solve(board.GetLength(0), board.GetLength(1), board, dictionaryWords);
        }

        /// <summary>
        /// Uses the board to find all possible words for boggle.
        /// <param name="dictionaryWords">The list of words imported from dictionary source.</param>
        /// <param name="board">The board that will be used for </param>
        /// </summary>
        public static HashSet<string> Solve(string[,] board, string[] dictionaryWords) {
            if (board == null) {
                throw new NullEmptyZeroException("Board is not initalized.");
            }
            return Solve(board.GetLength(0), board.GetLength(1), board, dictionaryWords);
        }

        /// <summary>
        /// Solves your boggle board using a list of words.
        /// <param name="dictionaryWords">The list of words imported from dictionary source.</param>
        /// <param name="board">The board that will be used for </param>
        /// <param name="width">The width of the board, must be greater than 0</param>
        /// <param name="height">The height of the board, must be greater than 0</param>
        /// </summary>
        public static HashSet<string> Solve(int width, int height, string[,] board, string[] dictionaryWords) {
            return Solve(width, height, board, new BoggleTree(dictionaryWords));
        }

        /// <summary>
        /// Solves your boggle board using a list of words.
        /// <param name="dictionaryWords">The tree of words imported from dictionary source.</param>
        /// <param name="board">The board that will be used for </param>
        /// <param name="width">The width of the board, must be greater than 0, if board equals null then board is initalized with width and height</param>
        /// <param name="height">The height of the board, must be greater than 0, if board equals null then board is initalized with width and height</param>
        /// </summary>
        public static HashSet<string> Solve(int width, int height, string[,] board, BoggleTree dictionaryWords) {
            if (width <= 0 || height <= 0) {
                throw new NullEmptyZeroException(width <= 0 ? height <= 0 ? "Width and Height are" : "Width is" : "Height is" + " less than or equal to 0.");
            }
            /// Creates new board if not yet created.
            if (board == null) { board = new string[height, width]; }

            /// If the board's dimensions do not equal the length or height, then
            /// a new board is created and all values are transfered to the new array.
            if (board.GetLength(0) != width || board.GetLength(1) != height) {
                string[,] temp = new string[height, width];
                int minw = Math.Min(board.GetLength(0), width);
                int minh = Math.Min(board.GetLength(1), height);
                for (int i = 0; i < minh; i++) {
                    for (int j = 0; j < minw; j++) {
                        temp[i, j] = board[i, j];
                    }
                }
                board = temp;
            }

            if (board.GetLength(0) == 0 || board.GetLength(1) == 0) {
                throw new NullEmptyZeroException("Board's " +
                    (board.GetLength(0) == 0 ? board.GetLength(1) == 0 ?
                    "width and height are" : "width is" : "height is") +
                    " less than or equal to 0.");
            }
            if (dictionaryWords == null || dictionaryWords.TotalWords() == 0) {
                throw new NullEmptyZeroException(
                    "Your dictionary is empty, you better go buy a new one.");
            }

            /// Sets the default dictionary if not initalized or if the dictionary is a newly imported file.
            if (_dictionaryTree == null || !_dictionaryTree.Equals(dictionaryWords)) {
                _dictionaryTree = dictionaryWords;
            }
            // If board had just been initalized, adds random characters to the board. This will also add random
            // characters if values have not been previously added to array.
            board = BoggleDice.RandomDice(board);

            // Creates a boggle board, links board together with neighbors.
            BoggleBoard boggleBoard = new BoggleBoard(board);
            // Discovered words will be placed into discoveredWords.
            HashSet<string> discoveredWords = new HashSet<string>();


            // Searches through the tree using a recursive algorithm that
            // backtracks once it hits a leaf node or if none of the combinations will
            // return an existing word. It will recurse only if it finds a prefix to a
            // word or if finds a prefix word that is also an existing word (ie. abandon
            // would be a word and the prefix to the word abandonment).
            foreach (BoggleNode piece in boggleBoard.neighborsOf.Keys) {
                HashSet<BoggleNode> frontier = new HashSet<BoggleNode>();
                frontier.Add(piece);
                BoggleSearch.RecursiveSearch(piece, piece.Character, frontier, boggleBoard.neighborsOf, _dictionaryTree, discoveredWords);
            }
            return discoveredWords;
        }


        /// <summary>
        /// Josef Nosov
        /// 
        /// A search algorithm that finds words within a tree in O(n*m) time, it is similar to
        /// depth first search, but uses recursion with backtracking to visit all possible nodes.
        /// </summary>
        private class BoggleSearch {
            /// <summary>
            /// Recursively searches through the tree in O(n*m) time, where n is the neighbors of the current
            /// node and m is the length of the word that we are searching for in the tree.
            /// </summary>
            /// <param name="currentState">The current node you are in within the board.</param>
            /// <param name="currentString">The word that is being matched to the dictionary</param>
            /// <param name="frontier">The current frontier, which nodes have been explored and can be ignored.</param>
            /// <param name="neighborsOf">Lists all the neigbors of the particular node.</param>
            /// <param name="dictionaryTree">The dictionary of words</param>
            /// <param name="foundWords">A list of words that have been found when running the
            /// search, only the words that have been found in the dictionary will be added.</param>
            public static void RecursiveSearch(BoggleNode currentState, string currentString, HashSet<BoggleNode> frontier,
                Dictionary<BoggleNode, HashSet<BoggleNode>> neigborsOf, BoggleTree dictionaryTree, HashSet<string> foundWords) {
                foreach (BoggleNode child in neigborsOf[currentState]) {
                    string childString = currentString + child.Character;
                    if (!frontier.Contains(child)) {
                        BoggleTree.SearchResults searchResult = dictionaryTree.Search(childString);
                        if (searchResult == BoggleTree.SearchResults.PrefixAndExists || searchResult == BoggleTree.SearchResults.Prefix) {
                            if (searchResult == BoggleTree.SearchResults.PrefixAndExists && childString.Length > 3) {
                                foundWords.Add(childString);
                            }
                            HashSet<BoggleNode> currentFrontier = new HashSet<BoggleNode>(frontier);
                            currentFrontier.Add(child);
                            RecursiveSearch(child, childString, currentFrontier, neigborsOf, dictionaryTree, foundWords);
                        } else {
                            if (searchResult == BoggleTree.SearchResults.Exists && childString.Length > 3) {
                                foundWords.Add(childString);
                            }
                        }
                    }
                }
            }
        }
    }
}
