﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BoggleApplication.Exceptions;

namespace BoggleApplication {

    /// <summary>
    /// Josef Nosov
    /// BoggleDice class is a utility class that provides
    /// randomness to the user's boggle dice.
    /// </summary>
    public class BoggleDice {
        /// <summary>
        /// Provides randomness to the roles.
        /// </summary>
        private static Random _rand = new Random();

        /// <summary>
        /// The list of characters created from the RandomBoggleCharacters method.
        /// </summary>
        private static string[] _randomCharacters = RandomBoggleCharacters();

        /// <summary>
        /// Creates an array of string based on the characters provided.
        /// </summary>
        /// <returns>Returns the array of boggle characters.</returns>
        private static string[] RandomBoggleCharacters() {
            /// I wanted to give some variation, vowels need to have a higher chance of showing up, whereas
            /// characters like z or x will rarely come up.
            char[] frequency = "aaaaaabbccdddeeeeeeeeeeeffgghhhhhiiiiiijkllllmmnnnnnnoooooooppqrrrrrsssssstttttttttuuuvvwwwxyyyz".ToCharArray();
            List<string> boggleRoles = new List<string>();
            foreach (char dice in frequency) {
                /// If q shows up, then add "qu", based on Boggles rules.
                if (dice == 'q') {
                    boggleRoles.Add("qu");
                } else {
                    boggleRoles.Add(Char.ToString(dice));
                }
            }
            return boggleRoles.ToArray();
        }

        /// <summary>
        /// Will either place random characters into the board or will add random values if
        /// any spot on the board is empty.
        /// <param name="dies">The board that is being manipulated</param>
        /// </summary>
        internal static string[,] RandomDice(string[,] dies) {
            if (dies == null || dies.GetLength(0) == 0 || dies.GetLength(1) == 0)
                throw new NullEmptyZeroException("The board has not been initalized properly.");
            for (int i = 0; i < dies.GetLength(0); i++) {
                for (int j = 0; j < dies.GetLength(1); j++) {
                    if (dies[i, j] == null) dies[i, j] = RandomChar();
                }
            }
            return dies;
        }

        /// <summary>
        /// Selects a variable at random from the list of characters.
        /// </summary>
        /// <returns>Returns a random character.</returns>
        internal static string RandomChar() {
            return _randomCharacters[_rand.Next(0, _randomCharacters.Length)];
        }

    }
}