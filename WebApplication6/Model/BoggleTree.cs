﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BoggleApplication.Exceptions;

namespace BoggleApplication {

    /// <summary>
    /// Josef Nosov
    /// 
    /// Data structure similar to a trie, uses a character rather than the whole word.
    /// Example when given the words { hat, hi, hit, yes }, the period indicates value
    /// is an existing word.
    ///             root
    ///             /  \
    ///             h   y
    ///            / \   \
    ///           a  .i   e
    ///          /     \   \ 
    ///        .t      .t  .s
    ///        
    /// </summary>
    public class BoggleTree {
        /// <summary>
        /// Possible results for searching.
        /// </summary>
        public enum SearchResults { NotExists, Prefix, Exists, PrefixAndExists };

        /// <summary>
        /// The root node of the tree.
        /// </summary>
        private Node _rootNode;

        /// <summary>
        /// Total words within the tree (similar to Length)
        /// </summary>
        private int _totalWords;

        /// <summary>
        /// Regular expression to check for only english alphabetic characters
        /// </summary>
        private Regex _alphabeticRegEx;

        /// <summary>
        /// Default Constructor, takes in no values and initalizes the tree.
        /// </summary>
        public BoggleTree() {
            _rootNode = new Node();
            _totalWords = 0;
            _alphabeticRegEx = new Regex("[^a-zA-Z]");
        }

        /// <summary>
        /// Takes in a dictionary of words and adds it to the data structure.
        /// </summary>
        /// <param name="dictionaryWords">Adds all the values within the dictionary to the tree.</param>
        public BoggleTree(string[] dictionaryWords)
            : this() {
            if (dictionaryWords == null || dictionaryWords.Length <= 0)
                throw new NullEmptyZeroException("Dictionary is empty. Buy a better dictionary.");
            foreach (string word in dictionaryWords) {
                Add(word);
            }
        }

        /// <summary>
        /// Adds a new word to the tree if word does not exist.
        /// If word does exist, then it is not added to the tree.
        /// If the word is empty or null, it is not added
        /// to the tree. The value also runs through a regular
        /// expression so any non-english and non-alphabetic 
        /// characters are eliminated.
        /// </summary>
        /// <param name="word">adds the word to the tree.</param>
        public void Add(string word) {
            if (!string.IsNullOrEmpty(word)) {
                Node selectedNode = _rootNode;
                char[] characters = CreateCharacterArray(word);
                if (characters.Length > 0) {
                    foreach (char character in characters) {
                        selectedNode = selectedNode.AddChild(character);
                    }
                    /// This is the end of word, which means that it is a complete word.
                    if (!selectedNode.IsWord) {
                        selectedNode.IsWord = true;
                        _totalWords++;
                    }
                }
            }
        }

        /// <summary>
        /// Searches for the word within the tree in O(n) runtime.
        /// </summary>
        /// <param name="word">The word we are searching for.</param>
        /// <returns>Returns results, 4 possible options: The word exists,
        /// The word doesn't exist, the current word is a prefix to
        /// another word or the current word is both an existing word
        /// and a prefix to another word.</returns>
        public SearchResults Search(string word) {

            if (string.IsNullOrEmpty(word)) { return SearchResults.NotExists; }
            word = _alphabeticRegEx.Replace(word.ToLower(), "");
            if (word == string.Empty) { return SearchResults.NotExists; }
            Node selectedNode = _rootNode;
            foreach (char character in word.ToCharArray()) {
                selectedNode = selectedNode.GetChild(character);
                if (selectedNode == null) { return SearchResults.NotExists; }
            }
            SearchResults searchResult;
            if (selectedNode.ChildrenCount == 0) {
                searchResult = SearchResults.Exists;
            } else {
                if (selectedNode.IsWord) {
                    searchResult = SearchResults.PrefixAndExists;
                } else {
                    searchResult = SearchResults.Prefix;
                }
            }
            return searchResult;
        }

        /// <summary>
        /// Returns the number of words. Similar to Length,
        /// it determines how many words are in the tree.
        /// </summary>
        /// <returns>The total number of words.</returns>
        public int TotalWords() {
            return _totalWords;
        }

        /// <summary>
        /// Creates a character array using regular expression, making sure the word will match.
        /// </summary>
        /// <param name="word">The word that will be converted to an array of characters</param>
        /// <returns>An array of characters from the word.</returns>
        private char[] CreateCharacterArray(string word) {
            return _alphabeticRegEx.Replace(word.ToLower(), "").ToCharArray();
        }


        /// <summary>
        /// The node of the tree. Holds information about the character within that node.
        /// </summary>
        class Node {

            /// <summary>
            /// Determines wether this node holds a word or is a prefix for another word.
            /// </summary>
            public bool IsWord;

            /// <summary>
            /// All the direct children of this node.
            /// </summary>
            private Dictionary<char, Node> _children;

            /// <summary>
            /// Initalizes the node.
            /// </summary>
            public Node() {
                _children = new Dictionary<char, Node>();
            }

            /// <summary>
            /// Adds the to the children of this node if child node
            /// does not exist. Returns the child node.
            /// </summary>
            /// <param name="child">The character within the child.</param>
            /// <returns>Returns the child node.</returns>
            public Node AddChild(char child) {
                Node childNode = new Node();
                if (!_children.ContainsKey(child)) {
                    _children.Add(child, childNode);
                } else {
                    childNode = _children[child];
                }
                return childNode;
            }

            /// <summary>
            /// If child exist, returns the child, if it doesn't exist, returns null.
            /// </summary>
            /// <param name="child">The character of the node you are looking for.</param>
            /// <returns>Returns the child if found or null if not.</returns>
            public Node GetChild(char child) {
                if (_children.ContainsKey(child)) {
                    return _children[child];
                }
                return null;
            }

            /// <summary>
            /// The list of children belonging to this node.
            /// </summary>
            public int ChildrenCount {
                get { return _children.Count; }
            }

        }
    }
}
