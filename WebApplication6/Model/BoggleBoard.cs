﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoggleApplication.Exceptions;

namespace BoggleApplication {

    /// <summary>
    /// Josef Nosov
    /// 
    /// Finds all possible direct neighbors of each node and places the
    /// values into a dictionary of hashmaps.
    /// </summary>
    class BoggleBoard {
        /// <summary>
        /// Hashmap of neigbors of a given node.
        /// </summary>
        public Dictionary<BoggleNode, HashSet<BoggleNode>> neighborsOf;

        /// <summary>
        /// Creates a boggle board and connects the neighbor nodes.
        /// </summary>
        /// <param name="the_array">The array of characters.</param>
        public BoggleBoard(string[,] the_array) {
            if (the_array == null || the_array.GetLength(0) == 0 || the_array.GetLength(1) == 0) {
                throw new NullEmptyZeroException("Board has not been initalized properly");
            }
            neighborsOf = new Dictionary<BoggleNode, HashSet<BoggleNode>>();
            /// An array of neighbors with a distance of one. All the evens values are Xs and
            /// the odd values are Ys.
            int[] neighborDistance = { -1, -1, -1, 0, -1, 1, 0, -1, 0, 1, 1, -1, 1, 0, 1, 1 };
            BoggleNode[,] nodes = new BoggleNode[the_array.GetLength(0), the_array.GetLength(1)];

            /// Creates the nodes.
            for (int i = 0; i < nodes.GetLength(0); i++) {
                for (int j = 0; j < nodes.GetLength(1); j++) {
                    nodes[i, j] = new BoggleNode(the_array[i, j]);
                    neighborsOf.Add(nodes[i, j], new HashSet<BoggleNode>());
                }
            }

            /// Links all the neighbots of the nodes. Ensures that values do not go out of bounds.
            for (int i = 0; i < nodes.GetLength(0); i++) {
                for (int j = 0; j < nodes.GetLength(1); j++) {
                    for (int k = 0; k < neighborDistance.Length; k += 2) {
                        int testRow = i + neighborDistance[k];
                        int testCol = j + neighborDistance[k + 1];
                        if (testRow >= 0 && testRow < nodes.GetLength(0) && testCol >= 0 && testCol < nodes.GetLength(1))
                            neighborsOf[nodes[i, j]].Add(nodes[i + neighborDistance[k], j + neighborDistance[k + 1]]);
                    }
                }
            }
        }
    }

    /// <summary>
    /// The Boggle Node, holds a character within the node.
    /// </summary>
    public class BoggleNode {
        private string _character;
        public string Character {
            get { return _character; }
        }
        public BoggleNode(string character) {
            if (string.IsNullOrEmpty(character)) {
                throw new NullEmptyZeroException("String must be at least one character long.");
            }
            _character = character;
        }

    }
}
